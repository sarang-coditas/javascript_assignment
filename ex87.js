class Animal{
    constructor(name, age){
        this.name = name;
        this.age = age;
    }
    eat(){
        return `${this.name} is eating`;
    }

    isSuperCute(){
        return this.age <=1;
    }
    isCute(){
        return true;
    }
}

// const animal1 = new Animal("Dog", 1);
// console.log(animal1.isSuperCute());

// dog class
class Dog extends Animal{
}

const animal2 = new Dog("Tommy", 1);
console.log(animal2.eat());