// array push pop

let fruits = ["apple", "mango", "grapes"];
console.log(fruits);
// push
fruits.push("banana");
console.log(fruits);

// pop
fruits.pop()
console.log(fruits);

// array unshift
fruits.unshift("banana")
console.log(fruits);

// array shift
fruits.shift();
console.log(fruits);