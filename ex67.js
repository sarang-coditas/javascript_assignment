// Sets (are iterable)
// store data
// sets also have its own methods
// No index based access
// Order is not guaranteed 
// Unique items only (no duplicates allowed)

// const numbers = new Set();
// numbers.add(1);
// numbers.add(2);
// numbers.add(3);
// numbers.add(4);
// numbers.add(5);

// if(numbers.has(1)){
//     console.log("1 is Present");
// }else{
//     console.log("1 is not present");
// }

// for(let number of numbers){
//     console.log(number);
// }
// console.log(numbers);

const myArray = [1,2,4,4,5,5,6];
const uniqueElement = new Set(myArray);
console.log(uniqueElement);

let length = 0;
for(let element of uniqueElement){
    length++;
}
console.log(length);