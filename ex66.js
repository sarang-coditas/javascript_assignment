// iterables
// strings, array are iterable

// const firstName = "Sarang";
// for(let char of firstName){
//     console.log(char);
// }

// const items = ['item1', 'item2', 'item3'];
// for(let item of items){
//     console.log(item);
// }

// array like object
// which have length property
// which can be accessed by index
// eg: string

// const firstName = "Sarang";
// console.log(firstName.length);
// console.log(firstName[2]);



