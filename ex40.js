// spread operator in objects

const obj1 = {
    key1: "value1",
    key2: "value2"
};
const obj2 = {
    key3: "value3",
    key4: "value4"
};

// const newObj = {...obj1, ...obj2};
// const newObj = {...["item1", "item2"]};
// const newObj = {..."abcdefghijklmnopqrstuvwxyz"};
console.log(newObj);
