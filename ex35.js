// array destructuring 
const myArray = ["value1", "value2", "value3", "value4"];

let [myvar1, myvar2, ...myNewArray] = myArray;
console.log("Value of myvar1", myvar1);
console.log("Value of myvar1", myvar2);
console.log(myNewArray);