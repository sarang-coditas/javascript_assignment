// truthy and falsy values

// falsy values
// false
// ""
// null
// undefined
// 0

let firstName = "Sarang"; // truthy

if(firstName){
    console.log(firstName);
}else{
    console.log("FirstName is kinda empty");
}