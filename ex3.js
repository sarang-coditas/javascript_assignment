//variables cannot start with numbers 
// 1value (invalid naming)
// first Name (invalid)


var value1 = 10;
console.log(value1);

// valid naming convections
// first_name
// $firstName
// variable names start with small letters
// cannot use * & % 

// first_name = "Sarang"; //snake case writing
// firstName = "Sarang"; //camelCasing