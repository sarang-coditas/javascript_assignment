// nested if else

// winning number 19
// 19 your guess is right
// 17 too low
// 28 too high

let winninNumber = 19;
let userGuess = Number(prompt("Guess a Number"));

if(userGuess === 19){
    console.log("Your guess is right");
}else{
    if(userGuess < winninNumber){
        console.log("Too low!!!");
    }else{
        console.log("Too high!!!");
    }
}
