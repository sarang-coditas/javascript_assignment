// param destructuring

// object
// react

const person = {
    firstName: "Sarang",
    gender: "Male",
}

function printDetails({firstName, gender}){
    console.log(firstName);
    console.log(gender);
}

printDetails(person);