// objects inside array
// very useful in real world applications

const users = [
    {userID: 1, firstName: "Sarang", gender: "male"},
    {userID: 2, firstName: "mohan", gender: "male"},
    {userID: 3, firstName: "nitish", gender: "male"},
];
for(let user of users){
    console.log(user.firstName);
};