// important array methods

const numbers = [2,3,4,5];

// function multiplyBy2(number, index){
//     console.log(`Index is ${index} number is ${number}`);
//     console.log(`${number}*2 = ${number*2}`);
// }

// for(let i=0; i<numbers.length; i++){
//     multiplyBy2(numbers[i], i)
// }

// numbers.forEach(multiplyBy2);

// numbers.forEach(function(number){
//     console.log(number*3);
// })

const users = [
    {firstName: "Sarang", age:23},
    {firstName: "Mohit", age:22},
    {firstName: "Harshit", age:21},
    {firstName: "Atharva", age:20}
]

users.forEach((user) => {
    console.log(user.firstName);
})

// users.forEach(function(user){
//     console.log(user.firstName);
// })