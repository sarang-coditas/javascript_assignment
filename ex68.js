// Maps
// map is an iterable

// store data in ordered fashion

// store key value pair(like object)
// duplicate keys are not allowed like objects

// different between maps and objects

// objects can onlu have string or symbol as key

// in maps you can use anything as key
// like array, number, string

const person = new Map();
person.set('firstName', 'Sarang');
person.set('age', 23);
person.set(1, 'one');
// console.log(person);
// console.log(person.get(1));
// for(let key of person.keys()){
//     console.log(key, typeof key);
// }

// for(let [key, value] of person){
//     console.log(key, value);
// }

const person1 = {
    id: 1,
    firstName: "Harshit" 
}
const person2 = {
    id: 2,
    firstName: "Harshita"
}

const extraInfro = new Map();
extraInfro.set(person1, {age: 8, gender: 'Male'});
extraInfro.set(person2, {age: 9, gender: 'Female'});
console.log(extraInfro);
