function createUser(firstName, lastName, email, age, address) {
    const user = {};
    user.firstName = firstName;
    user.lastName = lastName;
    user.email = email;
    user.age = age;
    user.address = address;
    user.about = userMethods.about;
    user.is18 = userMethods.is18;
    return user;
}

const userMethods = {
    about: function() {
        console.log(this.firstName, this.age);
    },
    is18: function(){
        return this.age >= 18;
    }
}

const user1 = createUser("Sarang", "Kulkarni", 'abc@example.com', 23, "my address");
console.log(user1.about());
