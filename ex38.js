// iterate object
const key ="email";
const person = {name: "Sarang",
                age: 23,
                "person hobbies": ["Cooking", "Reading"]
            };

// for in loop

// for(let key in person){
//     console.log(`${key} : ${person[key]}`);
// }

// Object.keys
// console.log(Object.keys(person));

for(let key of Object.keys(person)){
    console.log(person[key]);
}