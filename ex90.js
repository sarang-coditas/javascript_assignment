// getter and setters

class Person{
    constructor(firstName, lastName, age){
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }
    get fullName(){
        return `${this.firstName} ${this.lastname}.`;
    }
    set fullName(fullName){
        const [firstName, lastName] = fullName.split(" ");
        this.firstName = firstName;
        this.lastname = lastName;
    }
}

const person1 = new Person("Sarang", "Kulkarni", 23);
// console.log(person1.fullName);
console.log(person1.firstName);
console.log(person1.lastName);
person1.fullName = "Gaurav Kolhe";
console.log(person1.fullName);