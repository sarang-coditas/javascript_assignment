let firstName = "Sarang"; // declare variable using let

let firstName = "Mohit"; // cannot use 'let' (invalid) while redeclaring and assigning but can use 'var'

console.log(firstName);