
// function to create more users
// creates object
// add key, value pair
// return obj

function createUser(firstName, lastName, email, age, address) {
    const user = {};
    user.firstName = firstName;
    user.lastName = lastName;
    user.email = email;
    user.age = age;
    user.address = address;
    user.about = function() {
        console.log(this.firstName, this.age);
    }
    user.is18 = function(){
        return this.age >= 18;
    }
    return user;
}

const user1 = createUser("Sarang", "Kulkarni", 'abc@example.com', 23, "my address");
console.log(user1);
console.log(user1.is18());
console.log(user1.about());