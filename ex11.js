// undefined
// null

// let firstName;
// console.log(typeof firstName);
// firstName  ="Sarang";
// console.log(typeof firstName, firstName);

// let myVariable = null;
// console.log(myVariable);
// myVariable = "Sarang";
// console.log(myVariable, typeof myVariable);

console.log(typeof null); // prints 'object' its a bug


// BigInt
let myNumber = BigInt(1231234567891234567891234567);
let sameMyNumber = 123n;
console.log(myNumber + sameMyNumber);