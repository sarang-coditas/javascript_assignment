// methods
// function inside object

function personInfo() {
    console.log(`person name is ${this.firstName} and age is ${this.age}`);
}

const person1 = {
    firstName: "Sarang",
    age: 23,
    about: personInfo
}
const person2 = {
    firstName: "Gaurav",
    age: 23,
    about: personInfo
}
const person3 = {
    firstName: "Atharva",
    age: 23,
    about: personInfo
}

console.log(person1.about());
console.log(person2.about());
console.log(person3.about());

