const user1 = {
    firstName: "Sarang",
    age: 23,
    about: function(hobby, favMusician) {
        console.log(this.firstName, this.age, hobby, favMusician);
    }
}
const user2 = {
    firstName: "Gaurav",
    age: 22,
    
}
user1.about.call(user2,"Guitar", "KK");

// apply
user1.about.apply(user1, ["guitar", "KK"]);

// bind
const func = user1.about.bind(user2,"Guitar", "KK");
func();