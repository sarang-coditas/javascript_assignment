// function expression

// function welcomeMsg(){
//     console.log("Welcome to Coditas!");
// };

const welcomeMsg = function(){
    console.log("Welcome to Coditas!");
}
welcomeMsg();

const sumThreeNumbers = function(num1, num2, num3){
    return num1 + num2 + num3;
};
sumThreeNumbers(1,2,3);

const isEven = function(num){
    return num % 2 === 0;
};
console.log(isEven(4));

const fisrtChar = function(str1){
    return str1[0];
};

console.log(fisrtChar("Sarang"));

const target = function(array, target){
    for(let i=0; i<array.length; i++){
        if(array[i]==target){
            return i;
        }
    };
    return -1;
};
console.log(target([1,3,8,90], 8));