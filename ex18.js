// if
// else if
// else if
// else if
// else

let tempInDegree = 15;

if(tempInDegree < 0){
    console.log("extremely cold outside");
}else if(tempInDegree < 16){
    console.log("It is cold outside");
}else if(tempInDegree < 25){
    console.log("Weather is okay");
}else if(tempInDegree < 35){
    console.log("Let's go for a swim");
}else if(tempInDegree < 45){
    console.log("Turn on AC");
}else {
    console.log("Too hot!");
}

console.log("Hello");