class Animal{
    constructor(name, age){
        this.name = name;
        this.age = age;
    }
    eat(){
        return `${this.name} is eating`;
    }

    isSuperCute(){
        return this.age <=1;
    }
    isCute(){
        return true;
    }
}

// const animal1 = new Animal("Dog", 1);
// console.log(animal1.isSuperCute());

// dog class
class Dog extends Animal{
    constructor(name, age, speed){
        super(name, age);
        this.speed = speed;
    }
    sleep(){
        console.log(`${this.name} is sleeping`);
    }
    run() {
        console.log(`${this.name} running at ${this.speed}kmph`);
    }
}

const animal2 = new Dog("Tommy", 1, 30);
console.log(animal2.run());