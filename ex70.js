// optional chaining

const user = {
    firstName: "harshit",
    // address: {houseNumber: "3"}
}
console.log(user?.firstName);
console.log(user?.address?.houseNumber);
