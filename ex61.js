// find method

const myArray = ["Sarang", "Hello", "cat", "dog"];

function isLength(string){
    return string.length === 3;
}
const ans = myArray.find((string)=>string.length===3);
console.log(ans);

const users = [
    {userId: 1, userName: "sarang"},
    {userId: 2, userName: "harsh"},
    {userId: 3, userName: "nitish"},
    {userId: 4, userName: "gaurav"},
    {userId: 5, userName: "atharva"}
];

const myUser = users.find((user)=>user.userId===3);
console.log(myUser);