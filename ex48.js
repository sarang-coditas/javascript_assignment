// functions inside function

const app = () => {
    const myFunc = () => {
        console.log("myFunc");
    }
    const mul = (num1, num2) => num1 * num2;
    console.log("inside app");
    myFunc();
    console.log(mul(4,6));
}
app();
