// difference between dot and bracket notation

const key ="email";
const person = {name: "Sarang",
                age: 23,
                "person hobbies": ["Cooking", "Reading"]
            };

// console.log(person["person hobbies"]);

person[key] = "abc@example.com";
console.log(person);