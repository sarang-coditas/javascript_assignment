// every method
// const numbers = [2,4,6,8,10];

// const evenNum = numbers.every((number)=>number%2==0);
// console.log(evenNum);

// every method --> true if all true vice versa for false

// callback function --> true / false (boolean)

const userCart = [
    {productId: 1, productName: "mobile", price: 12000},
    {productId: 2, productName: "laptop", price: 22000},
    {productId: 3, productName: "Tv", price: 15000},
]

// check every product < 30000

const price = userCart.every((cartItem)=>cartItem.price < 30000);
console.log(price);