// sort method

// js sorts arrays as strings

// const numbers = [5, 9, 1200, 400, 3000];
// numbers.sort();
// console.log(numbers);

// const userNames = ['sarang', 'gaurav', 'atharva'];
// userNames.sort();
// console.log(userNames);

const numbers = [5, 9, 1200, 400, 3000];

numbers.sort((a,b)=>a-b);
console.log(numbers);

const products = [
    {productId: 1, productName: "p1", price: 3400},
    {productId: 2, productName: "p2", price: 3100},
    {productId: 3, productName: "p3", price: 3500},
    {productId: 4, productName: "p4", price: 30},
    {productId: 5, productName: "p5", price: 3000}
]

// lowToHigh
const lowToHigh = products.slice(0).sort((a,b)=>{
    return a.price-b.price
});

console.log(lowToHigh);

// highToLow
const highToLow = products.slice(0).sort((a,b)=>{
    return b.price-a.price
});

console.log(highToLow);