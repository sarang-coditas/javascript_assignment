// nested destructuring
const users = [
    {userID: 1, firstName: "Sarang", gender: "male"},
    {userID: 2, firstName: "mohan", gender: "male"},
    {userID: 3, firstName: "nitish", gender: "male"},
];

const [{firstName: user1firstName}, , {gender}] = users;
console.log(user1firstName, gender);