// js function ===> function + object

function hello() {
    console.log("Hello Coditas!");
}

// only functions have prototype

if(hello.prototype){
    console.log("Prototype Present");
}else{
    console.log("Prototype not present");
}

hello.prototype.abc = "abc";
hello.prototype.sing = function(){
    return "As it was!";
};
console.log(hello.prototype.sing());