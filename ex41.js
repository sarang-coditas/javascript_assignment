// object destructuring

const band = {
    bandName: "led zeppelin",
    famousSong: "stairway to heaven",
    year: "1998",
};

const {bandName, famousSong, ...restprops} = band;
console.log(bandName, restprops);