// objects // reference type
// arrays are good but not sufficient
// for real world data
// objects store key value pairs
// objects don't have index

// how to create objects

const person = {name: "Sarang",
                age: 23,
                hobbies: ["Cooking", "Reading"]};

// console.log(person, typeof person);

// how to access data from objects
// console.log(person.name, person.hobbies);



// how to add key value pair to objects
// person.gender = "male";
 person["gender"] = "male";
console.log(person);