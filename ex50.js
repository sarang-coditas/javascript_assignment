// block scope vs function scope

// let and const are block scope
// var is function scope

function myApp(){
    if(true){
        const firstName = "Sarang";
        console.log(firstName);
    }
    console.log(firstName);
}
myApp();