// some method

const numbers = [3,5,8,9];

// check if any number even

// const ans = numbers.some((number)=>number%2===0);
// console.log(ans);

const userCart = [
    {productId: 1, productName: "mobile", price: 12000},
    {productId: 2, productName: "laptop", price: 22000},
    {productId: 3, productName: "Tv", price: 15000},
]

// check if any product whose price is more than 20000

const ans = userCart.some((cartItem)=>cartItem.price>20000);
console.log(ans);