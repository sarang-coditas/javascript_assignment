// functions

function welcomeMsg(){
    console.log("Welcome to Coditas!");
};

// welcomeMsg();

// function twoPlusFour(){
//     return 2+4;
// };

// console.log(twoPlusFour());

function sumThreeNumbers(num1, num2, num3){
    return num1 + num2 + num3;
};

console.log(sumThreeNumbers(2, 4, 6));

// isEven

function isEven(num){
    return num % 2 === 0;
};
console.log(isEven(4));

// input as a string, output first character

function fisrtChar(str1){
    return str1[0];
};

console.log(fisrtChar("Sarang"));

// function to find index of target

function target(array, target){
    for(let i=0; i<array.length; i++){
        if(array[i]==target){
            return i;
        }
    };
    return -1;
};
console.log(target([1,3,8,90], 8));