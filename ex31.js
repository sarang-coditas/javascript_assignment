// use const for creating array

const fruits = ["apple", "mango"];

// array functions still work with const
fruits.push("banana");
console.log(fruits);
