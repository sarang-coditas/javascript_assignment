// ternary operator

// let age = 8;
// let drink;

// if(age>=5){
//     drink = "Coffee";
// }else {
//     drink = "Milk";
// }

// console.log(drink);

// ternary operator

let age = 3;
let drink = age >= 5 ? "coffee" : "Milk";
console.log(drink);