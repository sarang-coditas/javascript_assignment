// // new keyword

// function createUser(firstName, age) {
//     this.firstName = firstName;
//     this.age = age;
// };

// // new keyword
// // 1:) empty object like this = {}
// // 2:) return this
// const user1 = new createUser("Sarang", 23);
// console.log(user1);

function CreateUser(firstName, lastName, email, age, address) {
    this.firstName = firstName
    this.lastName = lastName;
    this.email = email;
    this.age = age;
    this.address = address;
    return this;
}
const user1 = new CreateUser("Sarang", "Kulkarni", 'abc@example.com', 23, "my address");

CreateUser.prototype.about = function() {
         console.log(this.firstName, this.age);  
};
CreateUser.prototype.is18 = function() {
    return this.age >= 18;
};
CreateUser.prototype.sing = function(){
    return 'As it Was';
};

console.log(user1.about());
console.log(user1.is18());
console.log(user1.sing());
