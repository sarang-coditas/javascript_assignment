// data types (premitive data types)
// string "Sarang"
// number 2, 4, 6, 8
// booleans
// undefined
// null
// BigInt
// Symbol

let age = 22;
let firstName = 'Sarang';
//console.log(typeof(age));

// convert number to string
// 22 -> "22"
// age = age + "";
// console.log(typeof(age));

// convert from string to number

// let myStr = +"34";
// console.log(typeof(myStr));

age = String(age);
console.log(typeof(age));