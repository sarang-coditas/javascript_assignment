function CreateUser(firstName, lastName, email, age, address) {
    this.firstName = firstName
    this.lastName = lastName;
    this.email = email;
    this.age = age;
    this.address = address;
    return this;
}
const user1 = new CreateUser("Sarang", "Kulkarni", 'abc@example.com', 23, "my address");

CreateUser.prototype.about = function() {
         console.log(this.firstName, this.age);  
};
CreateUser.prototype.is18 = function() {
    return this.age >= 18;
};
CreateUser.prototype.sing = function(){
    return 'As it Was';
};

console.log(user1.about());
console.log(user1.is18());
console.log(user1.sing());

for (let key in user1){
    if(user1.hasOwnProperty(key)){
        console.log(key);
    }
}