// and or operator

let firstName = "Sarang";
let age = 7;

// if(firstName[0] === 'S'){
//     console.log("Your name starts with S");
// }

// if(age > 18){
//     console.log("You are abohve 18");
// }

// if(firstName[0] === 'S' && age > 18){
//     console.log("Name starts with S and above 18");
// }else{
//     console.log("Else statement here");
// }

if(firstName[0] === 'S' || age > 18){
    console.log("Name starts with S and above 18");
}else{
    console.log("Else statement here");
}