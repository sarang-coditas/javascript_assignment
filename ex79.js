function createUser(firstName, lastName, email, age, address) {
    const user = Object.create(userMethods);
    user.firstName = firstName
    user.lastName = lastName;
    user.email = email;
    user.age = age;
    user.address = address;
    return user;
}

const userMethods = {
    about: function() {
        console.log(this.firstName, this.age);
    },
    is18: function(){
        return this.age >= 18;
    },
    sing: function() {
        return 'As it Was';
    }
}

const user1 = createUser("Sarang", "Kulkarni", 'abc@example.com', 23, "my address");
console.log(user1.about());
console.log(user1.sing());
