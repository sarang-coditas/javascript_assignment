"use strict"; // with use strict we cannot use undeclared variables

var firstName = "Sarang"; //declare and assign variable

console.log(firstName);
firstName = 'Mohit';
console.log(firstName);