// arrow functions

// function welcomeMsg(){
//     console.log("Welcome to Coditas!");
// };

const welcomeMsg = () => {
    console.log("Welcome to Coditas!");
}
welcomeMsg();

const sumThreeNumbers = (num1, num2, num3) => {
    return num1 + num2 + num3;
}
sumThreeNumbers(1,2,3);

const isEven = (num) => num % 2 === 0;
console.log(isEven(4));

const fisrtChar = (str1) => str1[0];

console.log(fisrtChar("Sarang"));

const target = (array, target) => {
    for(let i=0; i<array.length; i++){
        if(array[i]==target){
            return i;
        }
    };
    return -1;
}
console.log(target([1,3,8,90], 8));