// boolean and comparison operator

let num1 = '7';
let num2 = 7;

// console.log(num1 >= num2); // boolean returns either true of false

// == vs ===
// console.log(num1 == num2); // checks value only
// console.log(num1 === num2); // checks datatype and compares

// != vs !==

console.log(num1 !== num2);
