// lexical scope

const myVar = "value1";
function myApp(){

    function myFunc(){

        
        const myFunc2 = function(){
            console.log("inside my func", myVar);
        }
        myFunc2();
    }


    const myFunc3 = function(){}
    console.log(myVar);
    myFunc();
}

myApp();