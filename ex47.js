// hoisting

hello();

function hello(){
    console.log("Hello!");
}

// function call before declaration works only with function declaration and not expression or arrow funciton