// static methods and properties

class Person{
    constructor(firstName, lastName, age){
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }
    get fullName(){
        return `${this.firstName} ${this.lastname}.`;
    }
    set fullName(fullName){
        const [firstName, lastName] = fullName.split(" ");
        this.firstName = firstName;
        this.lastname = lastName;
    }
    static classInfo(){
        return "This is person class.";
    }
    static desc = "static property";
}

const person1 = new Person("Sarang", "Kulkarni");
console.log(Person.classInfo());
console.log(Person.desc);
