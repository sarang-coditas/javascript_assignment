const user1 = {
    firstName: "Sarang",
    age: 23,
    about() {
        console.log(this.firstName, this.age);
    }
}

user1.about();